const express = require('express')
const hbs = require('hbs')
const app = express()

app.set('view engine', 'hbs')
const port = 2023

app.get('/', (req, res) => {
    res.render('main');

})

app.get('/weather/:city?', async (req, res) => {
    let city = req.params.city;
    if(!city){
        res.redirect("/weather/Zhytomyr")
        return;
    }
    let key = 'fb209d39eee8e0767141bac8ec378eaa';
    let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${key}&units=metric`;
    let response = await fetch(url);
    let weather = await response.json();

    res.render(`weather`, {city, weather});
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})